PLUGIN_OS ?= linux
PLUGIN_ARCH ?= amd64

prepare_hisak:
	go mod tidy && \
	M=pkg/plugins/glauth-hisak make pull-plugin-dependencies

plugin_hisak: prepare_hisak bin/$(PLUGIN_OS)$(PLUGIN_ARCH)/hisak.so

bin/$(PLUGIN_OS)$(PLUGIN_ARCH)/hisak.so: pkg/plugins/glauth-hisak/*.go
	GOOS=$(PLUGIN_OS) GOARCH=$(PLUGIN_ARCH) go build ${TRIM_FLAGS} -ldflags "${BUILD_VARS}" -buildmode=plugin -o $@ $^
