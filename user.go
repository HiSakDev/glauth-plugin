package main

import (
	"fmt"
	"path/filepath"
	"strconv"

	"github.com/GehirnInc/crypt"
	_ "github.com/GehirnInc/crypt/md5_crypt"
	_ "github.com/GehirnInc/crypt/sha256_crypt"
	_ "github.com/GehirnInc/crypt/sha512_crypt"
	"github.com/glauth/glauth/v2/pkg/config"
	"github.com/glauth/ldap"
)

type linuxUser struct {
	backend     *backendData
	Name        string                       `json:"name"`
	UID         int                          `json:"uid"`
	GID         int                          `json:"gid"`
	CN          string                       `json:"cn"`
	Groups      []int                        `json:"groups"`
	Description string                       `json:"description"`
	Gecos       string                       `json:"gecos"`
	Home        string                       `json:"home"`
	Shell       string                       `json:"shell"`
	Shadow      string                       `json:"shadow"`
	Sudo        *sudoRole                    `json:"sudo"`
	Samba       *sambaRole                   `json:"samba"`
	Extra       map[string]unionData[string] `json:"extra"`
}

func (u linuxUser) GetName() string {
	if u.CN != "" {
		return u.CN
	}
	return u.Name
}

func (u linuxUser) GetHomeDirectory() string {
	if u.Home != "" {
		return u.Home
	}
	return filepath.Join("/home", u.GetName())
}

func (u linuxUser) GetLoginShell() string {
	if u.Shell != "" {
		return u.Shell
	}
	return "/bin/sh"
}

func (u linuxUser) GetConfig() config.User {
	cfg := config.User{
		Name:          u.GetName(),
		UnixID:        u.UID,
		UIDNumber:     u.UID,
		PrimaryGroup:  u.GID,
		OtherGroups:   u.Groups,
		Homedir:       u.GetHomeDirectory(),
		LoginShell:    u.GetLoginShell(),
		PassAppCustom: u.authenticator,
	}
	return cfg
}

func (u linuxUser) authenticator(user *config.User, pw string) error {
	if u.Shadow == "" {
		return nil
	}
	c := crypt.NewFromHash(u.Shadow)
	return c.Verify(u.Shadow, []byte(pw))
}

func (u linuxUser) ldapEntry(baseDN string) *ldap.Entry {
	if u.UID <= 0 {
		if u.Samba != nil {
			return u.Samba.ldapDomainEntry(baseDN)
		}
		return nil
	}
	name := u.GetName()
	primaryGroup := ""
	if group := u.backend.GetLinuxGroupById(u.GID); group != nil {
		primaryGroup = fmt.Sprintf("ou=%s,", group.GetName())
	}
	entry := &ldap.Entry{
		DN: fmt.Sprintf("cn=%s,%sou=users,%s", name, primaryGroup, baseDN),
		Attributes: []*ldap.EntryAttribute{
			{
				Name:   "objectClass",
				Values: []string{"top", "posixAccount", "shadowAccount"},
			},
			{
				Name:   "cn",
				Values: []string{name},
			},
			{
				Name:   "uid",
				Values: []string{name},
			},
			{
				Name:   "uidNumber",
				Values: []string{strconv.Itoa(u.UID)},
			},
			{
				Name:   "gidNumber",
				Values: []string{strconv.Itoa(u.GID)},
			},
			{
				Name:   "homeDirectory",
				Values: []string{u.GetHomeDirectory()},
			},
			{
				Name:   "loginShell",
				Values: []string{u.GetLoginShell()},
			},
		},
	}
	if u.Description != "" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "description",
			Values: []string{u.Description},
		})
	}
	if u.Gecos != "" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "gecos",
			Values: []string{u.Gecos},
		})
	}
	if u.Samba != nil {
		if sid := u.backend.GetSID(u.Samba.GetRID(u.UID)); sid != "" {
			entry.Attributes[0].Values = append(entry.Attributes[0].Values, "sambaSamAccount")
			entry.Attributes = append(entry.Attributes, u.Samba.ldapSambaEntryAttributes(sid, u.Gecos)...)
		}
	}
	for k, v := range u.Extra {
		if k == "objectClass" {
			entry.Attributes[0].Values = append(entry.Attributes[0].Values, v.RawData()...)
		} else {
			entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
				Name:   k,
				Values: v.RawData(),
			})
		}
	}
	return entry
}

func (u linuxUser) ldapSudoEntry(baseDN string) *ldap.Entry {
	if u.Sudo == nil {
		return nil
	}
	if u.UID == 0 {
		return u.Sudo.ldapEntry("defaults", baseDN)
	}
	return u.Sudo.ldapEntry(u.GetName(), baseDN)
}
