FROM golang:1.21-bullseye as builder

ENV CGO_ENABLED=1 \
    TRIM_FLAGS=-trimpath
WORKDIR /build
RUN git clone --depth 1 -b v2.3.2 https://github.com/glauth/glauth.git .
COPY . v2/pkg/plugins/glauth-hisak
RUN make -C v2 plugin_hisak linuxamd64

FROM ubuntu:24.04

COPY --from=builder /build/v2/bin/linuxamd64/hisak.so /app/
COPY --from=builder /build/v2/bin/linuxamd64/glauth /app/
RUN apt-get update \
    && apt-get install -y ldap-utils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
ENTRYPOINT ["/app/glauth"]