package main

import (
	"fmt"
	"strconv"

	"github.com/glauth/ldap"
)

type sudoRole struct {
	User       unionData[string]  `json:"user"`
	Host       unionData[string]  `json:"host"`
	Command    unionData[string]  `json:"command"`
	Option     unionData[string]  `json:"option"`
	RunAsUser  unionData[string]  `json:"runAsUser"`
	RunAsGroup unionData[string]  `json:"runAsGroup"`
	NotBefore  unionData[string]  `json:"notBefore"`
	NotAfter   unionData[string]  `json:"notAfter"`
	Order      unionData[float64] `json:"order"`
}

func (s sudoRole) ldapEntry(cn, baseDN string) *ldap.Entry {
	entry := &ldap.Entry{
		DN: fmt.Sprintf("cn=%s,ou=sudoers,%s", cn, baseDN),
		Attributes: []*ldap.EntryAttribute{
			{
				Name:   "objectClass",
				Values: []string{"top", "sudoRole"},
			},
			{
				Name:   "cn",
				Values: []string{cn},
			},
		},
	}
	if !s.User.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoUser",
			Values: s.User.RawData(),
		})
	} else if cn != "defaults" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoUser",
			Values: []string{cn},
		})
	}
	if !s.Host.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoHost",
			Values: s.Host.RawData(),
		})
	} else if cn != "defaults" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoHost",
			Values: []string{"ALL"},
		})
	}
	if !s.Command.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoCommand",
			Values: s.Command.RawData(),
		})
	} else if cn != "defaults" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoCommand",
			Values: []string{"ALL"},
		})
	}
	if !s.Option.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoOption",
			Values: s.Option.RawData(),
		})
	}
	if !s.RunAsUser.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoRunAsUser",
			Values: s.RunAsUser.RawData(),
		})
	}
	if !s.RunAsGroup.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoRunAsGroup",
			Values: s.RunAsGroup.RawData(),
		})
	}
	if !s.NotBefore.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoNotBefore",
			Values: s.NotBefore.RawData(),
		})
	}
	if !s.NotAfter.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "sudoNotAfter",
			Values: s.NotAfter.RawData(),
		})
	}
	if !s.Order.IsEmpty() {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name: "sudoOrder",
			Values: s.Order.StringData(func(f float64) string {
				return strconv.FormatFloat(f, 'f', -1, 64)
			}),
		})
	}
	return entry
}
