package main

import (
	"context"
	"fmt"
	"net"

	"github.com/GeertJohan/yubigo"
	"github.com/glauth/glauth/v2/pkg/config"
	"github.com/glauth/glauth/v2/pkg/handler"
	"github.com/glauth/ldap"
	"github.com/rs/zerolog"
	"go.opentelemetry.io/otel/trace"
)

func NewHiSakHandler(opts ...handler.Option) handler.Handler {
	options := handler.NewOptions(opts...)
	logger := options.Logger.With().Str("plugin", "hisak").Logger()
	h := hisakHandler{
		backend:   options.Backend,
		ldohelper: options.LDAPHelper,
		tracer:    options.Tracer,
		log:       &logger,
		cfg:       options.Config,
	}
	if data, err := newBackendData(h.backend.Database); err != nil {
		panic(err)
	} else {
		h.data = data
		go h.data.Watch(h.log)
	}
	return h
}

type hisakHandler struct {
	backend   config.Backend
	ldohelper handler.LDAPOpsHelper
	tracer    trace.Tracer
	log       *zerolog.Logger
	cfg       *config.Config
	data      *backendData
}

func (h hisakHandler) GetBackend() config.Backend {
	return h.backend
}

func (h hisakHandler) GetLog() *zerolog.Logger {
	return h.log
}

func (h hisakHandler) GetCfg() *config.Config {
	return h.cfg
}

func (h hisakHandler) GetYubikeyAuth() *yubigo.YubiAuth {
	return nil
}

func (h hisakHandler) Bind(bindDN, bindSimplePw string, conn net.Conn) (ldap.LDAPResultCode, error) {
	ctx, span := h.tracer.Start(context.Background(), "handler.hisakHandler.Bind")
	defer span.End()
	return h.ldohelper.Bind(ctx, h, bindDN, bindSimplePw, conn)
}

func (h hisakHandler) Search(boundDN string, req ldap.SearchRequest, conn net.Conn) (ldap.ServerSearchResult, error) {
	ctx, span := h.tracer.Start(context.Background(), "handler.hisakHandler.Search")
	defer span.End()
	return h.ldohelper.Search(ctx, h, boundDN, req, conn)
}

func (h hisakHandler) Close(_ string, _ net.Conn) error {
	return nil
}

func (h hisakHandler) Add(_ string, _ ldap.AddRequest, _ net.Conn) (ldap.LDAPResultCode, error) {
	return ldap.LDAPResultInsufficientAccessRights, nil
}

func (h hisakHandler) Modify(_ string, _ ldap.ModifyRequest, _ net.Conn) (ldap.LDAPResultCode, error) {
	return ldap.LDAPResultInsufficientAccessRights, nil
}

func (h hisakHandler) Delete(_, _ string, _ net.Conn) (ldap.LDAPResultCode, error) {
	return ldap.LDAPResultInsufficientAccessRights, nil
}

func (h hisakHandler) FindUser(_ context.Context, userName string, searchByUPN bool) (bool, config.User, error) {
	if searchByUPN {
		return false, config.User{}, nil
	}
	user := h.data.GetLinuxUser(userName)
	if user == nil {
		h.log.Warn().Str("username", userName).Msg("FindUser failed - no such user")
		return false, config.User{}, nil
	}
	return true, user.GetConfig(), nil
}

func (h hisakHandler) FindGroup(_ context.Context, groupName string) (bool, config.Group, error) {
	if groupName == "users" {
		return true, config.Group{Name: groupName}, nil
	}
	group := h.data.GetLinuxGroup(groupName)
	if group == nil {
		h.log.Warn().Str("group", groupName).Msg("FindGroup failed - no such group")
		return false, config.Group{}, nil
	}
	return true, group.GetConfig(), nil
}

func (h hisakHandler) FindPosixAccounts(_ context.Context, _ string) ([]*ldap.Entry, error) {
	entries := h.data.ListPosixAccount(h.backend.BaseDN)
	if sudoEntries := h.data.ListSudoRole(h.backend.BaseDN); len(sudoEntries) > 0 {
		entries = append(entries, &ldap.Entry{
			DN: fmt.Sprintf("ou=sudoers,%s", h.backend.BaseDN),
			Attributes: []*ldap.EntryAttribute{
				{
					Name:   "ou",
					Values: []string{"sudoers"},
				},
				{
					Name:   "objectClass",
					Values: []string{"top", "organizationalUnit"},
				},
			},
		})
		entries = append(entries, sudoEntries...)
	}
	return entries, nil
}

func (h hisakHandler) FindPosixGroups(_ context.Context, _ string) ([]*ldap.Entry, error) {
	return h.data.ListPosixGroup(h.backend.BaseDN), nil
}
