package main

import (
	"fmt"
	"strconv"

	"github.com/glauth/glauth/v2/pkg/config"
	"github.com/glauth/ldap"
)

type linuxGroup struct {
	backend     *backendData
	Name        string                       `json:"name"`
	GID         int                          `json:"gid"`
	CN          string                       `json:"cn"`
	Description string                       `json:"description"`
	Members     []string                     `json:"members"`
	Sudo        *sudoRole                    `json:"sudo"`
	Extra       map[string]unionData[string] `json:"extra"`
}

func (g linuxGroup) GetName() string {
	if g.CN != "" {
		return g.CN
	}
	return g.Name
}

func (g linuxGroup) GetConfig() config.Group {
	cfg := config.Group{
		Name:      g.GetName(),
		UnixID:    g.GID,
		GIDNumber: g.GID,
	}
	return cfg
}

func (g linuxGroup) ldapEntry(baseDN string) *ldap.Entry {
	if g.GID <= 0 {
		return nil
	}
	name := g.GetName()
	entry := &ldap.Entry{
		DN: fmt.Sprintf("cn=%s,ou=groups,%s", name, baseDN),
		Attributes: []*ldap.EntryAttribute{
			{
				Name:   "objectClass",
				Values: []string{"top", "posixGroup"},
			},
			{
				Name:   "cn",
				Values: []string{name},
			},
			{
				Name:   "gidNumber",
				Values: []string{strconv.Itoa(g.GID)},
			},
		},
	}
	if g.Description != "" {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "description",
			Values: []string{g.Description},
		})
	}
	if members := g.backend.FindGroupMembers(g.GID); len(members) > 0 || len(g.Members) > 0 {
		entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
			Name:   "memberUid",
			Values: append(members, g.Members...),
		})
	}
	for k, v := range g.Extra {
		if k == "objectClass" {
			entry.Attributes[0].Values = append(entry.Attributes[0].Values, v.RawData()...)
		} else {
			entry.Attributes = append(entry.Attributes, &ldap.EntryAttribute{
				Name:   k,
				Values: v.RawData(),
			})
		}
	}
	return entry
}

func (g linuxGroup) ldapSudoEntry(baseDN string) *ldap.Entry {
	if g.Sudo == nil {
		return nil
	}
	return g.Sudo.ldapEntry(fmt.Sprintf("%%%s", g.GetName()), baseDN)
}
