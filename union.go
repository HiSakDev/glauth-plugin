package main

import (
	"encoding/json"
	"fmt"
)

type unionData[T float64 | string] struct {
	data []T
}

func (u *unionData[T]) UnmarshalJSON(b []byte) error {
	var data any
	if err := json.Unmarshal(b, &data); err != nil {
		return err
	}
	u.data = nil
	switch values := data.(type) {
	case T:
		u.data = append(u.data, values)
	case []any:
		for _, value := range values {
			switch val := value.(type) {
			case T:
				u.data = append(u.data, val)
			default:
				return fmt.Errorf("unexpected type %T", value)
			}
		}
	default:
		return fmt.Errorf("unexpected type %T", data)
	}
	return nil
}

func (u *unionData[T]) IsEmpty() bool {
	return len(u.data) == 0
}

func (u *unionData[T]) RawData() []T {
	return u.data
}

func (u *unionData[T]) StringData(fn func(T) string) []string {
	data := make([]string, len(u.data), cap(u.data))
	for i, d := range u.data {
		data[i] = fn(d)
	}
	return data
}
