package main

import (
	"fmt"
	"regexp"

	"github.com/glauth/ldap"
)

var (
	sambaPasswordMatch = regexp.MustCompile(`^[[:xdigit:]]{32}$`)
)

type sambaRole struct {
	Domain     string `json:"domain"`
	SID        string `json:"sid"`
	RID        int    `json:"rid"`
	LMPassword string `json:"lmPassword"`
	NTPassword string `json:"ntPassword"`
}

func (s sambaRole) GetRID(id int) int {
	if s.RID > 0 {
		return s.RID
	}
	return id
}

func (s sambaRole) ldapSambaEntryAttributes(sid, name string) []*ldap.EntryAttribute {
	attrs := []*ldap.EntryAttribute{
		{
			Name:   "sambaSID",
			Values: []string{sid},
		},
		{
			Name:   "sambaAcctFlags",
			Values: []string{"[UX         ]"},
		},
	}
	if sambaPasswordMatch.MatchString(s.LMPassword) {
		attrs = append(attrs, &ldap.EntryAttribute{
			Name:   "sambaLMPassword",
			Values: []string{s.LMPassword},
		})
	}
	if sambaPasswordMatch.MatchString(s.NTPassword) {
		attrs = append(attrs, &ldap.EntryAttribute{
			Name:   "sambaNTPassword",
			Values: []string{s.NTPassword},
		})
	}
	if name != "" {
		attrs = append(attrs, &ldap.EntryAttribute{
			Name:   "displayName",
			Values: []string{name},
		})
	}
	return attrs
}

func (s sambaRole) ldapDomainEntry(baseDN string) *ldap.Entry {
	if s.Domain == "" || s.SID == "" {
		return nil
	}
	return &ldap.Entry{
		DN: fmt.Sprintf("sambaDomainName=%s,%s", s.Domain, baseDN),
		Attributes: []*ldap.EntryAttribute{
			{
				Name:   "objectClass",
				Values: []string{"top", "sambaDomain"},
			},
			{
				Name:   "sambaDomainName",
				Values: []string{s.Domain},
			},
			{
				Name:   "sambaSID",
				Values: []string{s.SID},
			},
		},
	}
}
