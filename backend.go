package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/glauth/ldap"
	"github.com/rs/zerolog"
)

type backendRole string

const (
	adminRole = backendRole("admin")
	userRole  = backendRole("user")
	groupRole = backendRole("group")
)

type backendData struct {
	db     string
	users  []*linuxUser
	groups []*linuxGroup
}

func newBackendData(db string) (*backendData, error) {
	f, err := os.Open(db)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var data backendData
	err = json.NewDecoder(f).Decode(&data)
	if err != nil {
		return nil, err
	}
	data.db = db
	return &data, nil
}

func (d *backendData) UnmarshalJSON(b []byte) error {
	var v map[string]struct {
		Role backendRole     `json:"role"`
		LDAP json.RawMessage `json:"ldap"`
	}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	for name, value := range v {
		if len(value.LDAP) == 0 {
			continue
		}
		switch value.Role {
		case adminRole:
			fallthrough
		case userRole:
			var u linuxUser
			if err := json.Unmarshal(value.LDAP, &u); err != nil {
				return err
			}
			u.backend = d
			u.Name = name
			d.users = append(d.users, &u)
		case groupRole:
			var g linuxGroup
			if err := json.Unmarshal(value.LDAP, &g); err != nil {
				return err
			}
			g.backend = d
			g.Name = name
			d.groups = append(d.groups, &g)
		default:
			return fmt.Errorf("unexpected role %s", value.Role)
		}
	}
	return nil
}

func (d *backendData) Watch(logger *zerolog.Logger) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logger.Fatal().Err(err).Msg("NewWatcher failed")
	}
	defer func() {
		logger.Warn().Err(watcher.Close()).Msg("watcher close")
	}()
	db, err := filepath.EvalSymlinks(d.db)
	if err != nil {
		logger.Fatal().Err(err).Msg("EvalSymlinks failed")
	}
	if err = watcher.Add(filepath.Dir(db)); err != nil {
		logger.Fatal().Err(err).Msg("watcher add failed")
	}
	logger.Info().Strs("dirs", watcher.WatchList()).Msg("watcher list")
	t := time.NewTicker(time.Second)
	t.Stop()
	count := 0
	for {
		select {
		case <-t.C:
			if backend, err := newBackendData(db); err != nil {
				logger.Err(err).Int("count", count).Msg("reload backend failed")
				if count++; count >= 10 {
					t.Stop()
				}
			} else {
				d.groups = backend.groups
				d.users = backend.users
				logger.Info().Int("count", count).
					Int("users", len(d.users)).
					Int("groups", len(d.groups)).
					Msg("reload backend")
				count = 0
				t.Stop()
			}
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			logger.Debug().Str("event", event.String()).Msg("watcher notify")
			if event.Name == db &&
				(event.Has(fsnotify.Chmod) || event.Has(fsnotify.Create) || event.Has(fsnotify.Write)) {
				t.Reset(time.Second)
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			logger.Err(err).Msg("watcher notify")
		}
	}
}

func (d *backendData) GetLinuxUser(name string) *linuxUser {
	for _, user := range d.users {
		if user.GetName() == name {
			return user
		}
	}
	return nil
}

func (d *backendData) GetLinuxGroup(name string) *linuxGroup {
	for _, group := range d.groups {
		if group.GetName() == name {
			return group
		}
	}
	return nil
}

func (d *backendData) GetLinuxGroupById(gid int) *linuxGroup {
	for _, group := range d.groups {
		if group.GID == gid {
			return group
		}
	}
	return nil
}

func (d *backendData) FindGroupMembers(gid int) []string {
	var members []string
	for _, user := range d.users {
		if slices.Contains(user.Groups, gid) {
			members = append(members, user.GetName())
		}
	}
	return members
}

func (d *backendData) ListPosixAccount(baseDN string) []*ldap.Entry {
	var entries []*ldap.Entry
	for _, user := range d.users {
		if entry := user.ldapEntry(baseDN); entry != nil {
			entries = append(entries, entry)
		}
	}
	return entries
}

func (d *backendData) ListPosixGroup(baseDN string) []*ldap.Entry {
	var entries []*ldap.Entry
	for _, group := range d.groups {
		if entry := group.ldapEntry(baseDN); entry != nil {
			entries = append(entries, entry)
		}
	}
	return entries
}

func (d *backendData) ListSudoRole(baseDN string) []*ldap.Entry {
	var entries []*ldap.Entry
	for _, user := range d.users {
		if entry := user.ldapSudoEntry(baseDN); entry != nil {
			entries = append(entries, entry)
		}
	}
	for _, group := range d.groups {
		if entry := group.ldapSudoEntry(baseDN); entry != nil {
			entries = append(entries, entry)
		}
	}
	return entries
}

func (d *backendData) GetSID(rid int) string {
	for _, user := range d.users {
		if user.Samba != nil && user.Samba.SID != "" {
			return fmt.Sprintf("%s-%d", user.Samba.SID, rid)
		}
	}
	return ""
}
