module gitlab.com/HiSakDev/glauth-plugin

go 1.21

require (
	github.com/GeertJohan/yubigo v0.0.0-20190917122436-175bc097e60e
	github.com/GehirnInc/crypt v0.0.0-20230320061759-8cc1b52080c5
	github.com/fsnotify/fsnotify v1.7.0
	github.com/glauth/glauth/v2 v2.3.2
	github.com/glauth/ldap v0.0.0-20240419171521-1f14f5c1b4ad
	github.com/rs/zerolog v1.33.0
	go.opentelemetry.io/otel/trace v1.27.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/boombuler/barcode v1.0.1-0.20190219062509-6c824513bacc // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/go-asn1-ber/asn1-ber v1.5.5 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/matttproud/golang_protobuf_extensions/v2 v2.0.0 // indirect
	github.com/pquerna/otp v1.4.0 // indirect
	github.com/prometheus/client_golang v1.18.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.45.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	github.com/rickb777/date v1.12.4 // indirect
	github.com/rickb777/plural v1.2.0 // indirect
	github.com/yaegashi/msgraph.go v0.1.4 // indirect
	go.opentelemetry.io/otel v1.27.0 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
